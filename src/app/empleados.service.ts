import { Injectable } from '@angular/core';
import { Empleado } from './empleado.model';
import { ServicioEmpleadosService } from './servicio-empleados.service';

@Injectable()
export class EmpleadosService {
  constructor(private servicioVentanaEmergente:ServicioEmpleadosService) { }

  empleados:Empleado[]=[
    new Empleado("David", "Alonso", "Developer", 1500), 
    new Empleado("Juan", "Gazpacho", "Scrum master", 2000),
    new Empleado("Antonio", "Martinez", "Developer", 2000),
    new Empleado("Sergio", "García", "Jefe", 5000), 
    new Empleado("Javier", "Baraja", "CTO", 100000),
    new Empleado("Sebastian", "Hernández", "Marketing", 1000),

  ];

  agregarEmpleadoServicio(empleado:Empleado){
    this.servicioVentanaEmergente.muestraMensaje("Persona que se va agregar: "+empleado.nombre);
    this.empleados.push(empleado);
  }

}

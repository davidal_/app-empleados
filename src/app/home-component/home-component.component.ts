import { Component, OnInit } from '@angular/core';
import { Empleado } from '../empleado.model';
import { EmpleadosService } from '../empleados.service';
import { ServicioEmpleadosService } from '../servicio-empleados.service';

@Component({
  selector: 'app-home-component',
  templateUrl: './home-component.component.html',
  styleUrls: ['./home-component.component.css']
})
export class HomeComponentComponent implements OnInit {
  titulo = 'Listado de empleados';
  empleados:Empleado[]=[];

  constructor(private miServicio:ServicioEmpleadosService, private servicioEmpleado:EmpleadosService) { 
    //this.empleados = this.servicioEmpleado.empleados;
  }

  ngOnInit(): void {
    this.empleados = this.servicioEmpleado.empleados;
  }
  
  agregarEmpleado(){
    let miEmpleado = new Empleado(this.cuadroNombre, this.cuadroApellidos, this.cuadroCargo, this.cuadroSalario);
    //this.miServicio.muestraMensaje("Nombre del empleado: "+miEmpleado.nombre)
    this.servicioEmpleado.agregarEmpleadoServicio(miEmpleado);
  }


  cuadroNombre:string="";
  cuadroApellidos:string="";
  cuadroCargo:string="";
  cuadroSalario:number=0;

  

}

export class Empleado{

    constructor(nombre:string, apellidos:string, cargo:string, salario:number){
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.cargo = cargo;
        this.salario = salario;
    }


    nombre:string = "";
    apellidos:string = "";
    salario:number = 0;
    cargo:string = "";

}